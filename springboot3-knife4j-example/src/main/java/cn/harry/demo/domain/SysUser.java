package cn.harry.demo.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Data
public class SysUser implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * ID
     */
    @Schema(description = "ID")
    private Long id;

    /**
     * 用户名
     */
    @Schema(description = "用户名")
    private String username;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * 性别 0 男 1 女 2 未知
     */
    @Schema(description = "性别 0 男 1 女 2 未知")
    private String sex;

    /**
     * 昵称
     */
    @Schema(description = "昵称")
    private String nickName;

    /**
     * 账号状态 0 禁用 1 启用
     */
    @Schema(description = "账号状态 0 禁用 1 启用")
    private String status;

    /**
     * 有效状态 0 无效 1 有效
     */
    @Schema(description = "有效状态 0 无效 1 有效")
    private Integer valid;


}