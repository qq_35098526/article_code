package cn.harry.demo.controller;

import cn.harry.demo.domain.SysUser;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
@Tag(name = "用户管理")
@RestController
public class UserController {

    @Operation(summary = "获取单个用户")
    @PostMapping("/user/info")
    public SysUser getUserInfo() {
        SysUser sysUser = new SysUser();
        sysUser.setId(1L);
        sysUser.setUsername("harry");
        sysUser.setSex("1");
        sysUser.setNickName("harry");
        return sysUser;
    }

    @Operation(summary = "获取所有用户")
    @PostMapping("/user/list")
    public List<SysUser> list() {
        List<SysUser> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            SysUser sysUser = new SysUser();
            sysUser.setId(i + 1L);
            sysUser.setUsername("harry" + i);
            sysUser.setSex("1");
            sysUser.setNickName("harry" + i);
            list.add(sysUser);
        }
        return list;
    }

}
