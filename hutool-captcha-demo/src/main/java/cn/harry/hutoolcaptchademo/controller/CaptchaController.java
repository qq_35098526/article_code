package cn.harry.hutoolcaptchademo.controller;

import cn.harry.hutoolcaptchademo.param.CaptchaValidateParam;
import cn.harry.hutoolcaptchademo.service.SysCaptchaService;
import cn.harry.hutoolcaptchademo.vo.CaptchaResult;
import cn.hutool.core.util.StrUtil;
import jakarta.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;


/**
 * @author harry
 */
@Slf4j
@RestController
@RequestMapping("/captcha")
@AllArgsConstructor
public class CaptchaController {

    private final SysCaptchaService sysCaptchaService;

    /**
     * 生成验证码
     */
    @GetMapping("/getCaptcha")
    public CaptchaResult getCaptcha(HttpSession session) {
        return sysCaptchaService.getCaptcha(session);
    }

    /**
     * 验证码校验
     *
     * @param param 验证码参数
     */
    @PostMapping("/check")
    public boolean checkCaptcha(@RequestBody CaptchaValidateParam param, HttpSession session) {
        String uuid = param.getUuid();
        String captcha = param.getCode();
        if (StrUtil.isEmpty(uuid) || StrUtil.isEmpty(captcha)) {
            log.error("验证码参数不能为空");
            return false;
        }
        log.info("接收到验证码: uuId:{}, 验证码:{}", uuid, captcha);
        // 参数校验
        return sysCaptchaService.validate(uuid, captcha, session);
    }
}