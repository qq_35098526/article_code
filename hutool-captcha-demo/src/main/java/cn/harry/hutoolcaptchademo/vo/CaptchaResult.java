package cn.harry.hutoolcaptchademo.vo;

import lombok.Builder;
import lombok.Data;

/**
 * @author harry
 */
@Builder
@Data
public class CaptchaResult {
    /**
     * 验证码缓存key
     */
    private String uuid;
    /**
     * 验证码图片Base64字符串
     */
    private String img;

}
