package cn.harry.hutoolcaptchademo.param;

import lombok.Builder;
import lombok.Data;

/**
 * @author harry
 */
@Builder
@Data
public class CaptchaValidateParam {
    /**
     * 图形验证码
     */
    private String code;

    /**
     * 唯一标识
     */
    private String uuid = "";

}
