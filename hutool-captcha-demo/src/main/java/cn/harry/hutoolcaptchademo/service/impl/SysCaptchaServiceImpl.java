package cn.harry.hutoolcaptchademo.service.impl;

import cn.harry.config.property.CaptchaProperties;
import cn.harry.hutoolcaptchademo.enums.CaptchaTypeEnums;
import cn.harry.hutoolcaptchademo.enums.Constants;
import cn.harry.hutoolcaptchademo.service.SysCaptchaService;
import cn.harry.hutoolcaptchademo.vo.CaptchaResult;
import cn.hutool.captcha.AbstractCaptcha;
import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.generator.CodeGenerator;
import cn.hutool.core.util.IdUtil;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.awt.*;

/**
 * 图片验证码
 *
 * @author harry
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SysCaptchaServiceImpl implements SysCaptchaService {

    private final CaptchaProperties captchaProperties;
    private final CodeGenerator codeGenerator;
    private final Font captchaFont;

    @Override

    public boolean validate(String uuid, String code) {
        // 从redis中获取验证码
        String redisCode = "redis存储的相关逻辑";
        return codeGenerator.verify(redisCode, code);
    }


    @Override
    public CaptchaResult getCaptcha() {

        String captchaType = captchaProperties.getType();
        int width = captchaProperties.getWidth();
        int height = captchaProperties.getHeight();
        int interfereCount = captchaProperties.getInterfereCount();
        int codeLength = captchaProperties.getCode().getLength();

        AbstractCaptcha captcha;
        if (CaptchaTypeEnums.CIRCLE.name().equalsIgnoreCase(captchaType)) {
            captcha = CaptchaUtil.createCircleCaptcha(width, height, codeLength, interfereCount);
        } else if (CaptchaTypeEnums.GIF.name().equalsIgnoreCase(captchaType)) {
            captcha = CaptchaUtil.createGifCaptcha(width, height, codeLength);
        } else if (CaptchaTypeEnums.LINE.name().equalsIgnoreCase(captchaType)) {
            captcha = CaptchaUtil.createLineCaptcha(width, height, codeLength, interfereCount);
        } else if (CaptchaTypeEnums.SHEAR.name().equalsIgnoreCase(captchaType)) {
            captcha = CaptchaUtil.createShearCaptcha(width, height, codeLength, interfereCount);
        } else {
            throw new IllegalArgumentException("Invalid captcha type: " + captchaType);
        }
        captcha.setGenerator(codeGenerator);
        captcha.setTextAlpha(captchaProperties.getTextAlpha());
        captcha.setFont(captchaFont);

        String code = captcha.getCode();
        String imageBase64Data = captcha.getImageBase64Data();

        // 验证码文本缓存至Redis，用于登录校验
        String uuid = IdUtil.fastSimpleUUID();

        // 将验证码存到redis中 并设置5分钟过期
        // RedisUtils.set(RedisKeys.getCaptchaKey(uuid), code, 5 * 60);

        return CaptchaResult.builder().img(imageBase64Data).uuid(uuid).build();
    }

    @Override
    public CaptchaResult getCaptcha(HttpSession session) {


        String captchaType = captchaProperties.getType();
        int width = captchaProperties.getWidth();
        int height = captchaProperties.getHeight();
        int interfereCount = captchaProperties.getInterfereCount();
        int codeLength = captchaProperties.getCode().getLength();

        AbstractCaptcha captcha;
        if (CaptchaTypeEnums.CIRCLE.name().equalsIgnoreCase(captchaType)) {
            captcha = CaptchaUtil.createCircleCaptcha(width, height, codeLength, interfereCount);
        } else if (CaptchaTypeEnums.GIF.name().equalsIgnoreCase(captchaType)) {
            captcha = CaptchaUtil.createGifCaptcha(width, height, codeLength);
        } else if (CaptchaTypeEnums.LINE.name().equalsIgnoreCase(captchaType)) {
            captcha = CaptchaUtil.createLineCaptcha(width, height, codeLength, interfereCount);
        } else if (CaptchaTypeEnums.SHEAR.name().equalsIgnoreCase(captchaType)) {
            captcha = CaptchaUtil.createShearCaptcha(width, height, codeLength, interfereCount);
        } else {
            throw new IllegalArgumentException("Invalid captcha type: " + captchaType);
        }
        captcha.setGenerator(codeGenerator);
        captcha.setTextAlpha(captchaProperties.getTextAlpha());
        captcha.setFont(captchaFont);

        String code = captcha.getCode();
        String imageBase64Data = captcha.getImageBase64Data();

        // 验证码文本缓存至Redis，用于登录校验
        String uuid = IdUtil.fastSimpleUUID();

        session.setAttribute(Constants.CAPTCHA_CODE, code);
        session.setAttribute(Constants.CAPTCHA_UUID, uuid);

        return CaptchaResult.builder().img(imageBase64Data).uuid(uuid).build();
    }

    @Override
    public boolean validate(String uuid, String code, HttpSession session) {
        // session中获取验证码
        String captchaCode = (String) session.getAttribute(Constants.CAPTCHA_CODE);
        return codeGenerator.verify(captchaCode, code);
    }
}