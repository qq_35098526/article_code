package cn.harry.hutoolcaptchademo.service;


import cn.harry.hutoolcaptchademo.vo.CaptchaResult;
import jakarta.servlet.http.HttpSession;

/**
 * 图片验证码
 *
 * @author harry
 */
public interface SysCaptchaService {

    /**
     * 验证码效验
     *
     * @param uuid uuid
     * @param code 验证码
     * @return true：成功 false：失败
     */
    boolean validate(String uuid, String code);

    /**
     * 获取验证码
     *
     * @return 验证码
     */
    CaptchaResult getCaptcha();

    /**
     * 获取验证码
     *
     * @param session
     * @return
     */
    CaptchaResult getCaptcha(HttpSession session);

    /**
     * 验证码效验
     *
     * @param uuid
     * @param code
     * @param session
     * @return
     */
    boolean validate(String uuid, String code, HttpSession session);
}
