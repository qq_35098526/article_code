package cn.harry.hutoolcaptchademo.enums;

/**
 * @author harry
 */
public class Constants {
    public static final String CAPTCHA_CODE = "code";
    public static final String CAPTCHA_UUID = "uuid";
}
