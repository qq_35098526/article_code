package cn.harry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

/**
 * @author harry
 */
@ConfigurationPropertiesScan
@SpringBootApplication
public class HutoolCaptchaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HutoolCaptchaDemoApplication.class, args);
    }

}
