package cn.harry.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author harry
 * @公众号 Harry技术
 * Spring Boot 3 集成 Spring Security（1） 认证: https://mp.weixin.qq.com/s/khW7yDuJF4j5Sbg9nx3KKQ
 */
@Slf4j
@Controller
public class LoginController {

    @GetMapping("/")
    public String home() {
        log.info("index");
        // 返回名为 "index" 的模板或 HTML 页面
        return "index";
    }

    @GetMapping("/mylogin")
    public String login() {
        // 返回名为 "mylogin" 的模板或 HTML 页面
        return "mylogin";
    }

    @GetMapping("/main")
    public String main() {
        // 返回名为 "main" 的模板或 HTML 页面
        return "main";
    }

}
