package cn.harry.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author harry
 * @公众号 Harry技术
 * Spring Boot 3 集成 Spring Security（2） 授权: https://mp.weixin.qq.com/s/HzzcYIQLnch_7r7wdUarew
 */
@Slf4j
@RestController
public class UserController {

    @GetMapping("/user/info")
    public User getUserInfo() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.info("当前登录的用户信息：{}", user.toString());
        return user;
    }

    @GetMapping("/user/info2")
    @PreAuthorize("hasRole('USER')") // 只有 USER 角色才能访问
    public User getUserInfo2() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.info("当前登录的用户信息：{}", user.toString());
        return user;
    }
}
