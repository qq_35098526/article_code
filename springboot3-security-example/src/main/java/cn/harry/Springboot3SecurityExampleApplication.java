package cn.harry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author harry
 */
@SpringBootApplication
public class Springboot3SecurityExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot3SecurityExampleApplication.class, args);
    }

}
