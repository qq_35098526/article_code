# article_code

文章配套示例代码

## 代码库

- Gitee
```
https://gitee.com/honghh/article_code
```
- Gitcode
```
https://gitcode.com/qq_35098526/article_code/overview
```
## 公众号

微信搜一搜：**Harry技术** 或扫描下方二维码关注

![Harry技术](https://raw.gitcode.com/qq_35098526/article_code/attachment/uploads/9d79b5a1-a8a8-4630-a08b-8e4e2f187fdb/qrcode_for_gh_13b4f294f738_344.jpg 'qrcode_for_gh_13b4f294f738_344.jpg')

## 博文地址

| 文章                                            | 公众号                                                      | CSDN                                                         | 掘金                                               |
| ----------------------------------------------- | ----------------------------------------------------------- | ------------------------------------------------------------ | -------------------------------------------------- |
| SpringBoot3整合Hutool-captcha实现图形验证码     | https://mp.weixin.qq.com/s/KxxiK8h-NQoVS8OmlESN2A | https://blog.csdn.net/qq_35098526/article/details/143946224|https://juejin.cn/post/7441972982879125504 |
| Spring Boot 3 集成 Spring Security（1）认证     | https://mp.weixin.qq.com/s/khW7yDuJF4j5Sbg9nx3KKQ           | https://blog.csdn.net/qq_35098526/article/details/144029420  | https://juejin.cn/post/7441457836670959668         |
| Spring Boot 3 集成 Spring Security（2）授权     | https://mp.weixin.qq.com/s/HzzcYIQLnch_7r7wdUarew           | https://blog.csdn.net/qq_35098526/article/details/144060563  | https://juejin.cn/post/7441467538259836968         |
| Spring Boot 3 集成 Spring Security（3）数据管理 | https://mp.weixin.qq.com/s/FRW7YKnF24da-vwSbVjX4Q           | https://blog.csdn.net/qq_35098526/article/details/144080725  | https://juejin.cn/post/7441768231211515944         |
| Spring Boot 3 整合Knife4j（OpenAPI3规范）       | https://mp.weixin.qq.com/s/R7p9ltx7MNlkNqHcVGQTUg           | https://blog.csdn.net/qq_35098526/article/details/144106886  | https://juejin.cn/post/7441998838565945359         |
| Spring Boot 3 整合Redis（1） 基础功能           | https://mp.weixin.qq.com/s/Yboe8pYS180Hc7_FwWeNJw           | https://blog.csdn.net/qq_35098526/article/details/144129379  | https://juejin.cn/post/7442347773818372136         |
| Spring Boot 3 整合Redis（2）注解驱动缓存        | https://mp.weixin.qq.com/s/8VYvjlXNG-DR67cO4_pPAw           | https://blog.csdn.net/qq_35098526/article/details/144129618  | https://juejin.cn/post/7442369456762273844         |
| Spring Boot 3 集成 Spring Security + JWT        | https://mp.weixin.qq.com/s/ZGKY-UoMVB1FKMdr5yAmqQ           | https://blog.csdn.net/qq_35098526/article/details/144204266  | https://juejin.cn/spost/7443790754059370511        |
|                                                 |                                                             |                                                              |                                                    |
|                                                 |                                                             |                                                              |                                                    |

