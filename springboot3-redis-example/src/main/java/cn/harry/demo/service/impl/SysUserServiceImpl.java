package cn.harry.demo.service.impl;

import cn.harry.demo.domain.SysUser;
import cn.harry.demo.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
@Service
public class SysUserServiceImpl implements SysUserService {

    @Override
    @CacheEvict(value = "user", allEntries = true)
    public void clear() {
        // 清除缓存中的所有数据
        log.info("clear ....");
    }

    @Override
    @CacheEvict(value = "user", key = "#p0")
    public void delete(Long id) {
        // 清除缓存中的指定用户数据
        log.info("delete ....  id:{}", id);
    }

    @Override
    @CachePut(value = "user", key = "#p0.id")
    public SysUser update(SysUser user) {
        log.info("更新数据，update ....  user:{}", user);
        return user;
    }

    @Override
    @Cacheable(value = "user", key = "#p0")
    public SysUser get(Long id) {
        // 模拟从数据库查询到的数据
        SysUser user = new SysUser();
        user.setId(id);
        user.setUsername("harry_" + id);
        log.info("获取到数据，get ....  id:{}", id);
        return user;
    }
}
