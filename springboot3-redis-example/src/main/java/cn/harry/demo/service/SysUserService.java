package cn.harry.demo.service;

import cn.harry.demo.domain.SysUser;

/**
 * @author harry
 * @公众号 Harry技术
 */
public interface SysUserService {
    void clear();

    void delete(Long id);

    SysUser update(SysUser user);

    SysUser get(Long id);

}
