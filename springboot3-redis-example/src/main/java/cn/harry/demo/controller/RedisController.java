package cn.harry.demo.controller;

import cn.harry.demo.domain.SysUser;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * @author harry
 * @公众号 Harry技术
 */
@RestController
@RequestMapping("/api/redis")
@RequiredArgsConstructor
public class RedisController {

    private final RedisTemplate<String, Object> redisTemplate;

    // set 数据
    @PostMapping("/set")
    public String set(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
        return "ok";
    }

    // get 数据
    @GetMapping("/get")
    public Object get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    // delete 数据
    @DeleteMapping("/delete")
    public Boolean delete(String key) {
        return redisTemplate.delete(key);
    }

    // set user 数据
    @PostMapping("/setUser")
    public String setUser(String key) {
        SysUser user = new SysUser();
        user.setId(1L);
        user.setUsername("harry");
        user.setNickName("Harry技术");
        redisTemplate.opsForValue().set(key, user);
        return "ok";
    }

    // get user 数据
    @GetMapping("/getUser")
    public SysUser getUser(String key) {
        return (SysUser) redisTemplate.opsForValue().get(key);
    }

}
