package cn.harry.demo.controller;

import cn.harry.demo.domain.SysUser;
import cn.harry.demo.service.SysUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
@RestController
@RequestMapping("/api/cache")
@RequiredArgsConstructor
public class CacheController {

    private final SysUserService sysUserService;

    @GetMapping("/get")
    public SysUser get(Long id) {
        log.info( "发起get请求 controller id:{}.", id);
        return sysUserService.get(id);
    }

    @PutMapping("/update")
    public SysUser update(SysUser sysUser) {
        log.info( "发起update请求 controller sysUser:{}.", sysUser);
        return sysUserService.update(sysUser);
    }

    @DeleteMapping("/delete")
    public void delete(Long id) {
        log.info( "发起delete请求 controller id:{}.", id);
        sysUserService.delete(id);
    }

    @DeleteMapping("/clear")
    public void clear() {
        log.info( "发起clear请求 controller.");
        sysUserService.clear();
    }
}
