package cn.harry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author harry
 * @公众号 Harry技术
 */
//启用缓存支持
@EnableCaching
@SpringBootApplication
public class SpringbootRedisExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootRedisExampleApplication.class, args);
    }

}
