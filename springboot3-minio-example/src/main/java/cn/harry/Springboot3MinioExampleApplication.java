package cn.harry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot3MinioExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot3MinioExampleApplication.class, args);
    }

}
