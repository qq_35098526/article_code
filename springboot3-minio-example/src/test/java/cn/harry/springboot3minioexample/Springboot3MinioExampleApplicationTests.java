package cn.harry.springboot3minioexample;

import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@SpringBootTest
class Springboot3MinioExampleApplicationTests {

    @Test
    void contextLoads() throws IOException, ServerException, InsufficientDataException, ErrorResponseException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        FileInputStream fileInputStream = new FileInputStream("E:\\minio.gif");
        // 创建一个minio的客户端
        MinioClient client = MinioClient.builder()
                .credentials("eFbF9rSBtgfqiLGDcPDo", "Sfgc06T1S6zhzVTzCwIFhr6KUs7zqCwOwFkHHez4")
                .endpoint("http://127.0.0.1:9090")
                .build();
        //上传
        PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                .object("minio.gif") //文件名称
                .contentType("text/html") //文件类型
                .bucket("harry") // 桶的名称
                .stream(fileInputStream, fileInputStream.available(), -1)
                .build();
        client.putObject(putObjectArgs);

        //访问路径
        System.out.println("http://127.0.0.1:9000/harry/minio.gif");
    }
}
