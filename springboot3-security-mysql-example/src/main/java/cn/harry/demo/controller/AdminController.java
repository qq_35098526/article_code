package cn.harry.demo.controller;

import cn.harry.component.security.model.SysUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author harry
 * @公众号 Harry技术
 * Spring Boot 3 集成 Spring Security（2） 授权: https://mp.weixin.qq.com/s/HzzcYIQLnch_7r7wdUarew
 */
@Slf4j
@RestController
public class AdminController {


    @GetMapping("/admin/info")
    @PreAuthorize("hasRole('ROOT')")  // 只有 ADMIN 角色才能访问
    public SysUserDetails adminInfo() {
        // 获取当前登录的用户信息
        SysUserDetails user = (SysUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.info("当前登录的用户信息：{}", user.toString());
        return user;
    }
}
