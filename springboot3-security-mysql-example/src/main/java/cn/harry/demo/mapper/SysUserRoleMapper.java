package cn.harry.demo.mapper;

import cn.harry.demo.domain.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.Set;

/**
 * @author harry
 * @公众号 Harry技术
*/
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {


    Set<String> listRoleKeyByUserId(Long useId);
}




