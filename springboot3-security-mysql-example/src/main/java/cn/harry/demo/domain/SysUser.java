package cn.harry.demo.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author harry
 * @公众号 Harry技术
 */
@TableName(value ="sys_user")
@Data
public class SysUser implements Serializable {
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别 0 男 1 女 2 未知
     */
    private String sex;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 账号状态 0 禁用 1 启用
     */
    private String status;

    /**
     * 有效状态 0 无效 1 有效
     */
    private Integer valid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}