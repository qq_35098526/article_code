package cn.harry.demo.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author harry
 * @公众号 Harry技术
 */
@TableName(value ="sys_role")
@Data
public class SysRole implements Serializable {
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 角色名
     */
    private String name;

    /**
     * 密码
     */
    private String code;

    /**
     * 状态 0 禁用 1 启用
     */
    private String status;

    /**
     * 有效状态 0 无效 1 有效
     */
    private Integer valid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}