package cn.harry.demo.service;

import cn.harry.demo.domain.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author harry
 * @公众号 Harry技术
*/
public interface SysRoleService extends IService<SysRole> {

}
