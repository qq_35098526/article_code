package cn.harry.demo.service;

import cn.harry.demo.domain.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author harry
 * @公众号 Harry技术
*/
public interface SysUserRoleService extends IService<SysUserRole> {

}
