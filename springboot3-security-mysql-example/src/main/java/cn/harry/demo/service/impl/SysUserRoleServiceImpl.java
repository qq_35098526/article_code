package cn.harry.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.harry.demo.domain.SysUserRole;
import cn.harry.demo.service.SysUserRoleService;
import cn.harry.demo.mapper.SysUserRoleMapper;
import org.springframework.stereotype.Service;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

}




