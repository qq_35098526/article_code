package cn.harry.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.harry.demo.domain.SysUser;
import cn.harry.demo.service.SysUserService;
import cn.harry.demo.mapper.SysUserMapper;
import org.springframework.stereotype.Service;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}




