package cn.harry.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.harry.demo.domain.SysRole;
import cn.harry.demo.service.SysRoleService;
import cn.harry.demo.mapper.SysRoleMapper;
import org.springframework.stereotype.Service;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

}




