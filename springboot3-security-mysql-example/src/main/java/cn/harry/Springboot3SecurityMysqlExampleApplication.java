package cn.harry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author harry
 *
 * @公众号 Harry技术
 */
@SpringBootApplication
public class Springboot3SecurityMysqlExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot3SecurityMysqlExampleApplication.class, args);
    }

}
