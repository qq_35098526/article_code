## 引入依赖

[MyBatis-Plus](https://baomidou.com/getting-started/install/) 是一个 MyBatis 的增强工具，在 MyBatis 的基础上只做增强不做改变，为简化开发、提高效率而生。

>注意事项
>
>版本 3.5.9+ 开始修改为可选依赖，具体查看下文 maven bom 部分。
```xml
  <mybatisplus.version>3.5.9</mybatisplus.version>
```
```xml
    <!-- MyBatis-Plus https://baomidou.com-->
    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-spring-boot3-starter</artifactId>
    </dependency>

    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-jsqlparser</artifactId>
    </dependency>
```

```xml
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-bom</artifactId>
                <version>${mybatisplus.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
```

>使用 maven bom 管理依赖，减少版本号的冲突。因为 jsqlparser 5.0+ 版本不再支持 jdk8 针对这个问题解耦 jsqlparser 依赖。 正确打开姿势，引入 mybatis-plus-bom 模块，然后引入 ..starter 和 ..jsqlparser.. 依赖 

## 表结构
```mysql
-- 用户表
CREATE TABLE `sys_user` (
	`id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'ID',
	`username` VARCHAR ( 64 ) DEFAULT NULL COMMENT '用户名',
	`password` VARCHAR ( 64 ) DEFAULT NULL COMMENT '密码',
	`sex` CHAR ( 1 ) DEFAULT '0' COMMENT '性别 0 男 1 女 2 未知',
	`nick_name` VARCHAR ( 64 ) DEFAULT NULL COMMENT '昵称',
	`status` CHAR ( 1 ) DEFAULT '1' COMMENT '账号状态 0 禁用 1 启用',
	`valid` INT DEFAULT '1' COMMENT '有效状态 0 无效 1 有效',
PRIMARY KEY ( `id` ) 
) ENGINE = INNODB COMMENT = '用户';

INSERT INTO `sys_user` (`id`, `username`, `password`, `sex`, `nick_name`, `status`, `valid`) VALUES (1, 'admin', '$2a$10$xZdonloiiL6YfoLZv6mrJuvxtD238uHPIKkVDpQKBuZxzMDpTf8uK', '0', '管理员张三', '1', 1);
INSERT INTO `sys_user` (`id`, `username`, `password`, `sex`, `nick_name`, `status`, `valid`) VALUES (2, 'user', '$2a$10$evM9SfvuN/E.ykWWOf6b3eTltPvuc6XjwW/qIhagSjlsTfi9l26Ba', '0', '用户李四', '1', 1);

-- 角色表
CREATE TABLE `sys_role` (
	`id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'ID',
	`name` VARCHAR ( 64 ) DEFAULT NULL COMMENT '角色名',
	`code` VARCHAR ( 64 ) DEFAULT NULL COMMENT '密码',
	`status` CHAR ( 1 ) DEFAULT '1' COMMENT '状态 0 禁用 1 启用',
	`valid` INT DEFAULT '1' COMMENT '有效状态 0 无效 1 有效',
PRIMARY KEY ( `id` ) 
) ENGINE = INNODB COMMENT = '角色';

INSERT INTO `sys_role` (`id`, `name`, `code`, `status`, `valid`) VALUES (1, '管理员', 'ROOT', '1', 1);
INSERT INTO `sys_role` (`id`, `name`, `code`, `status`, `valid`) VALUES (2, '普通用户', 'USER', '1', 1);


-- 用户角色关系表
CREATE TABLE `sys_user_role` (
	`id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = INNODB COMMENT = '用户角色关系表';
```
