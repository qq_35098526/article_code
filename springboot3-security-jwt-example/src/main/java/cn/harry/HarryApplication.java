package cn.harry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

/**
 * @author harry
 * @公众号 Harry技术
 */
@SpringBootApplication
@ConfigurationPropertiesScan
public class HarryApplication {

    public static void main(String[] args) {
        SpringApplication.run(HarryApplication.class, args);
    }

}
