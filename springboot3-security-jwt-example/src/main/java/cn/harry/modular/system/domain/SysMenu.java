package cn.harry.modular.system.domain;

import cn.hutool.core.date.DatePattern;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户权限表
 *
 * @author harry
 * @公众号 Harry技术
 */
@TableName(value = "sys_menu")
@Data
public class SysMenu implements Serializable {
    /**
     * 菜单ID
     */
    @Schema(description = "菜单ID")
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Long menuId;

    /**
     * 父级菜单id
     */
    @Schema(description = "父级菜单id")
    private Long pid;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 权限标识
     */
    @Schema(description = "权限标识")
    private String permission;

    /**
     * 图标
     */
    @Schema(description = "图标")
    private String icon;

    /**
     * 权限类型，0:目录 1:菜单 2:按钮（接口绑定权限）
     */
    @Schema(description = "权限类型，0:目录 1:菜单 2:按钮（接口绑定权限）")
    private Integer type;

    /**
     * 前端资源路径
     */
    @Schema(description = "前端资源路径")
    private String uri;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer sort;

    /**
     * 是否为外链， 0:否 1:是
     */
    @Schema(description = "是否为外链， 0:否 1:是")
    private Integer outerLink;

    /**
     * 路由地址
     */
    @Schema(description = "路由地址")
    private String path;

    /**
     * 启用状态，0:禁用 1:启用
     */
    @Schema(description = "启用状态，0:禁用 1:启用")
    private String status;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date createTime;

    /**
     * 有效状态，0:无效 1:有效
     */
    @Schema(description = "有效状态，0:无效 1:有效")
    @TableLogic
    private Integer valid;

    @Serial
    private static final long serialVersionUID = 1L;
}