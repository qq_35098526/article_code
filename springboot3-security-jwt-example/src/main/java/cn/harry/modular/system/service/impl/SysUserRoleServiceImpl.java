package cn.harry.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.harry.modular.system.domain.SysUserRole;
import cn.harry.modular.system.service.SysUserRoleService;
import cn.harry.modular.system.mapper.SysUserRoleMapper;
import org.springframework.stereotype.Service;

/**
* @author harry
* @公众号 Harry技术
*/
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole>
    implements SysUserRoleService{

}




