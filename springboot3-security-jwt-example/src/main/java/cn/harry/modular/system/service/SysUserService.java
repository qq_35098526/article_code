package cn.harry.modular.system.service;

import cn.harry.modular.system.domain.SysUser;
import cn.harry.modular.auth.vo.LoginResult;
import cn.harry.modular.auth.vo.UserInfoResult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author harry
 * @公众号 Harry技术
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * login
     *
     * @param username
     * @param password
     * @return
     */
    LoginResult login(String username, String password);

    /**
     * get user info
     *
     * @return
     */
    UserInfoResult getInfo();
}
