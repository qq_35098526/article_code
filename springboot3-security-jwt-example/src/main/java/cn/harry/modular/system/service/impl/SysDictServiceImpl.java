package cn.harry.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.harry.modular.system.domain.SysDict;
import cn.harry.modular.system.service.SysDictService;
import cn.harry.modular.system.mapper.SysDictMapper;
import org.springframework.stereotype.Service;

/**
* @author harry
* @公众号 Harry技术
*/
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict>
    implements SysDictService{

}




