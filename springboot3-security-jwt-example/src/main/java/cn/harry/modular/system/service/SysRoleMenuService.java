package cn.harry.modular.system.service;

import cn.harry.modular.system.domain.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author harry
* @公众号 Harry技术
*/
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}
