package cn.harry.modular.system.controller;

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.enums.BusinessType;
import cn.harry.modular.system.domain.SysDept;
import cn.harry.modular.system.service.SysDeptService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 部门管理
 *
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@Tag(name = "部门管理")
@RequestMapping("/sys_dept")
public class SysDeptController {
    private final SysDeptService sysDeptService;


    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('sys_dept_page')")
    @GetMapping(value = "/page")
    public R<IPage<SysDept>> page(@ParameterObject Page<SysDept> page, @ParameterObject SysDept sysDept) {
        return R.success(sysDeptService.page(page, Wrappers.lambdaQuery(sysDept)));
    }


    @Operation(summary = "详情")
    @PreAuthorize("@ss.hasPermission('sys_dept_get')")
    @GetMapping(value = "/{id}")
    public R<SysDept> getById(@PathVariable Long id) {
        return R.success(sysDeptService.getById(id));
    }


    @Operation(summary = "新增")
    @PreAuthorize("@ss.hasPermission('sys_dept_add')")
    @SysLog(title = "sys_dept", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> save(@RequestBody SysDept sysDept) {
        return sysDeptService.save(sysDept) ? R.success() : R.failed();
    }


    @Operation(summary = "更新")
    @PreAuthorize("@ss.hasPermission('sys_dept_edit')")
    @SysLog(title = "sys_dept", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> update(@RequestBody SysDept sysDept) {
        return sysDeptService.updateById(sysDept) ? R.success() : R.failed();
    }


    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('sys_dept_del')")
    @SysLog(title = "sys_dept", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return sysDeptService.removeBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }

}
