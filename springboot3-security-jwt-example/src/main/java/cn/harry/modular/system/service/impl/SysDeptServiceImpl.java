package cn.harry.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.harry.modular.system.domain.SysDept;
import cn.harry.modular.system.service.SysDeptService;
import cn.harry.modular.system.mapper.SysDeptMapper;
import org.springframework.stereotype.Service;

/**
* @author harry
* @公众号 Harry技术
*/
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept>
    implements SysDeptService{

}




