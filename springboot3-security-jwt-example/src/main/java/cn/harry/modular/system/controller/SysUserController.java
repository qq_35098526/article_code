package cn.harry.modular.system.controller;

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.enums.BusinessType;
import cn.harry.modular.system.domain.SysUser;
import cn.harry.modular.system.service.SysUserService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 用户管理
 *
* @author harry
* @公众号 Harry技术
*/
@Slf4j
@RestController
@RequiredArgsConstructor
@Tag(name = "用户管理")
@RequestMapping("/sys_user")
public class SysUserController{
    private final SysUserService sysUserService;


    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('sys_user_page')")
    @GetMapping(value = "/page")
    public R<IPage<SysUser>> page(@ParameterObject Page<SysUser> page, @ParameterObject  SysUser sysUser) {
        return R.success(sysUserService.page(page, Wrappers.lambdaQuery(sysUser)));
    }


    @Operation(summary = "详情")
    @PreAuthorize("@ss.hasPermission('sys_user_get')")
    @GetMapping(value = "/{id}")
    public R<SysUser> getById(@PathVariable Long id) {
        return R.success(sysUserService.getById(id));
    }


    @Operation(summary = "新增")
    @PreAuthorize("@ss.hasPermission('sys_user_add')")
    @SysLog(title = "sys_user", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> save(@RequestBody SysUser sysUser) {
        return sysUserService.save(sysUser) ? R.success() : R.failed();
    }

    @Operation(summary = "更新")
    @PreAuthorize("@ss.hasPermission('sys_user_edit')")
    @SysLog(title = "sys_user", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> update(@RequestBody SysUser sysUser) {
        return sysUserService.updateById(sysUser) ? R.success() : R.failed();
    }

    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('sys_user_del')")
    @SysLog(title = "sys_user", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return sysUserService.removeBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }

}
