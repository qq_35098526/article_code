package cn.harry.modular.system.controller;

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.enums.BusinessType;
import cn.harry.modular.system.domain.SysMenu;
import cn.harry.modular.system.service.SysMenuService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 菜单管理
 *
* @author harry
* @公众号 Harry技术
*/
@Slf4j
@RestController
@RequiredArgsConstructor
@Tag(name = "菜单管理")
@RequestMapping("/sys_menu")
public class SysMenuController{
    private final SysMenuService sysMenuService;


    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('sys_menu_page')")
    @GetMapping(value = "/page")
    public R<IPage<SysMenu>> page(@ParameterObject Page<SysMenu> page, @ParameterObject  SysMenu sysMenu) {
        return R.success(sysMenuService.page(page, Wrappers.lambdaQuery(sysMenu)));
    }


    @Operation(summary = "详情")
    @PreAuthorize("@ss.hasPermission('sys_menu_get')")
    @GetMapping(value = "/{id}")
    public R<SysMenu> getById(@PathVariable Long id) {
        return R.success(sysMenuService.getById(id));
    }


    @Operation(summary = "新增")
    @PreAuthorize("@ss.hasPermission('sys_menu_add')")
    @SysLog(title = "sys_menu", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> save(@RequestBody SysMenu sysMenu) {
        return sysMenuService.save(sysMenu) ? R.success() : R.failed();
    }


    @Operation(summary = "更新")
    @PreAuthorize("@ss.hasPermission('sys_menu_edit')")
    @SysLog(title = "sys_menu", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> update(@RequestBody SysMenu sysMenu) {
        return sysMenuService.updateById(sysMenu) ? R.success() : R.failed();
    }


    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('sys_menu_del')")
    @SysLog(title = "sys_menu", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return sysMenuService.removeBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }

}
