package cn.harry.modular.system.enums;

/**
 * EasyCaptcha 验证码类型枚举
 *
 * @author harry
 */
public enum CaptchaTypeEnums {

    /**
     * 圆圈干扰验证码
     */
    CIRCLE,
    /**
     * GIF验证码
     */
    GIF,
    /**
     * 干扰线验证码
     */
    LINE,
    /**
     * 扭曲干扰验证码
     */
    SHEAR
}
