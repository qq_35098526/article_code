package cn.harry.modular.system.service;

import cn.harry.modular.system.domain.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Set;

/**
* @author harry
* @公众号 Harry技术
*/
public interface SysUserRoleService extends IService<SysUserRole> {

}
