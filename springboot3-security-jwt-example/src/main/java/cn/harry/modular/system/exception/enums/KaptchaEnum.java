package cn.harry.modular.system.exception.enums;

import cn.harry.common.api.IErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 验证码异常枚举
 *
 * @author harry
 */
@Getter
@AllArgsConstructor
public enum KaptchaEnum implements IErrorCode {

	/**
	 * 参数不可以为空
	 */
	UUID_NOT_NULL(1401, "uuid不能为空"),
	MSG_CODE_ERROR(1402, "验证码错误"),;

	private final long code;

	private final String msg;


	@Override
	public long getCode() {
		return code;
	}


	@Override
	public String getMessage() {
		return msg;
	}


}
