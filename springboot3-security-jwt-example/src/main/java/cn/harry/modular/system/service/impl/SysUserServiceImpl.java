package cn.harry.modular.system.service.impl;

import cn.harry.common.exception.ApiException;
import cn.harry.component.security.constant.SecurityConstants;
import cn.harry.component.security.model.SysUserDetails;
import cn.harry.component.security.utils.JwtUtils;
import cn.harry.component.security.utils.SecurityUtils;
import cn.harry.config.property.SecurityProperties;
import cn.harry.modular.system.domain.SysUser;
import cn.harry.modular.system.mapper.SysUserMapper;
import cn.harry.modular.system.service.SysUserService;
import cn.harry.modular.auth.vo.LoginResult;
import cn.harry.modular.auth.vo.SysUserInfo;
import cn.harry.modular.auth.vo.UserInfoResult;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
* @author harry
* @公众号 Harry技术
*/
@Slf4j
@Service
@RequiredArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser>
    implements SysUserService{

    private final SecurityProperties securityProperties;
    private final AuthenticationManager authenticationManager;

    @Override
    public LoginResult login(String username, String password) {

        LoginResult res = new LoginResult();
        String accessToken;
        Long expiration = securityProperties.getJwt().getTtl();
        // 密码需要客户端加密后传递
        try {
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            SecurityContextHolder.getContext().setAuthentication(authenticate);

            // 认证成功后生成JWT令牌
            accessToken = JwtUtils.createToken(authenticate);

            res.setToken(accessToken);
            res.setExpiration(expiration);
            res.setTokenType(StrUtil.trim(SecurityConstants.JWT_TOKEN_PREFIX));

            return res;
        } catch (Exception e) {
            log.error("登录异常:{}", e.getMessage());
            throw new ApiException(e.getMessage());
        }
    }

    @Override
    public UserInfoResult getInfo() {
        SysUserDetails sysUserDetails = SecurityUtils.getSysUserDetails();
        SysUser user = sysUserDetails.getSysUser();
        UserInfoResult result = new UserInfoResult();
        result.setUsername(user.getUsername());
        result.setPermissions(sysUserDetails.getPermissions());
        result.setRoles(sysUserDetails.getRoles());
        result.setUserInfo(BeanUtil.copyProperties(user, SysUserInfo.class));
        return result;
    }
}




