package cn.harry.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.harry.modular.system.domain.SysMenu;
import cn.harry.modular.system.service.SysMenuService;
import cn.harry.modular.system.mapper.SysMenuMapper;
import org.springframework.stereotype.Service;

/**
* @author harry
* @公众号 Harry技术
*/
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu>
    implements SysMenuService{

}




