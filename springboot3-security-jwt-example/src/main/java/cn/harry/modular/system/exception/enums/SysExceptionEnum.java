package cn.harry.modular.system.exception.enums;

import cn.harry.common.api.IErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * ClassName: UmsExceptionEnum Description:
 *
 * @author harry Date 2019/08/30 11:22 Copyright (C) www.tech-harry.cn
 */
@Getter
@AllArgsConstructor
public enum SysExceptionEnum implements IErrorCode {

    /******** 系统管理 member 1001 *********/
    WRONG_USERNAME_OR_PASSWORD(1001, "用户名或密码错误"),
    USER_DISABLED(1002, "您的账号已被停用"),
    USER_NOT_EXIST(1003, "用户不存在"),

    /******** 菜单管理 menu 1101 *********/
    MENU_SUBMENU_EXISTS(1101, "存在子菜单,不允许删除"),
    MENU_IS_ASSIGNED(1102, "菜单已分配,不允许删除"),

    /******** 字典管理 dict 1201 *********/
    DICT_TYPE_EXISTS(1201, "字典类型已存在,不允许修改/添加"),

    /******** 部门管理 dept 1301 *********/
    DEPT_NOT_EXISTS(1301, "部门已停用或不存在，不允许新增"),
    ;

    private final long code;

    private final String msg;

    @Override
    public long getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return msg;
    }

}
