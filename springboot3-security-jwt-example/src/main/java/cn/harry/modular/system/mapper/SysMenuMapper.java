package cn.harry.modular.system.mapper;

import cn.harry.modular.system.domain.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.Set;

/**
* @author harry
* @公众号 Harry技术
*/
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    Set<String> getMenuPermission(Long useId);
}




