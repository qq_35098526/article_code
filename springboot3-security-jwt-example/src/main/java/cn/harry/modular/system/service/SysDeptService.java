package cn.harry.modular.system.service;

import cn.harry.modular.system.domain.SysDept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author harry
* @公众号 Harry技术
*/
public interface SysDeptService extends IService<SysDept> {

}
