package cn.harry.common.constant;

/**
 * 公共常量
 *
 * @author harry
 */
public class CommonConstant {
	/**
	 * 超级管理员角色
	 */
	public static final String SUPER_ADMIN_ROOT = "ROOT";

	/**
	 * 所有权限标识
	 */
	public static final String ALL_PERMISSION = "*:*:*";

}
