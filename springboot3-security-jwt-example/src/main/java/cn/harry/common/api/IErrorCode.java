package cn.harry.common.api;

/**
 * 封装API的错误码
 *
 * @author harry
 * @公众号 Harry技术
 */
public interface IErrorCode {

	long getCode();

	String getMessage();

}
